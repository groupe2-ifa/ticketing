<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Developer extends Model
{
    protected $fillable = [];

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }
}
