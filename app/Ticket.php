<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function emergency(){
        return $this->belongsTo(Emergency::class);
    }

    public function status(){
        return $this->belongsTo(Status::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function developer(){
        return $this->belongsTo(Developer::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function project(){
        return $this->belongsTo(Project::class);
    }
}
