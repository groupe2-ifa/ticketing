<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [];

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }
}
