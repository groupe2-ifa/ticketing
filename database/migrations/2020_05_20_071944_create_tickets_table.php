<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('content');
            $table->text('upload');
            $table->timestamp('transferDate')->nullable(); //date de prise en charge à rappeler la prochaine fois a chloé et jeremie
            $table->unsignedTinyInteger('id_emergency')->nullable();
            $table->unsignedTinyInteger('id_status')->nullable();
            $table->unsignedBigInteger('id_customer');
            $table->unsignedBigInteger('id_developer')->nullable();
            $table->unsignedTinyInteger('id_category');
            $table->unsignedBigInteger('id_project');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
